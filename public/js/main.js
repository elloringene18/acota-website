$('#bar-services .bar').on('click', function () {
    $('#bar-services .bar.active').removeClass('active');
    $(this).addClass('active');

    eq = $('#bar-services .bar').index($(this));
    $('.bar-content').hide();
    $('.bar-content').eq(eq).show();
});


function toggleBurger(x) {
    x.classList.toggle("change");
    $('#main-menu').toggle("active");
}


function vcenter() {
    $('.vcenter').each(function () {
        $(this).css('margin-top',-($(this).height()/2));
    })
}

$(window).on('resize',function () {
    setTimeout(function () {
        vcenter();
    },100);
});

$(document).ready(function () {
    setTimeout(function () {
        vcenter();
    },100);
});
