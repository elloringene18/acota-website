<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'ACCOUNTING',
                'content' => '<p>We offer managed solutions to help businesses run a streamlined and enhanced finance function. Our accounting service consists of four layers – Bookkeeping, Financial Accounting, Management Accounting and CFO Services.</p>',
                'content_bottom' => '',
                'items' => [
                    [
                        'title' => 'Bookkeeping:',
                        'content' => '<p>This is the minimum to keep your business ticking along with timely transaction processing, including payroll, accounts receivable and accounts payable, as well as production of cash-based financial statements each month. </p>',
                        'image' => 'img/our-services/book-keeping.png',
                    ],
                    [
                        'title' => 'Financial Accounting:',
                        'content' => '
                                    <p>
                                    Bookkeeping output to produce accrual-based financial statements based on applicable accounting standards.
                                     </p>
                        ',
                        'image' => 'img/our-services/financial-accounting.png',
                    ],
                    [
                        'title' => 'Management Accounting:',
                        'content' => '
                                    <p>
                                    Design, build and maintain budgets, forecasts and KPIs, as well as monthly management reports that are tailor-made to your business, including relevant variance analysis.
                                     </p>
                        ',
                        'image' => 'img/our-services/management-accounting.png',
                    ],
                    [
                        'title' => 'CFO Services:',
                        'content' => '
                                    <p>
                                    Working with your management team to guide decision-making and drive performance.
                                     </p>
                        ',
                        'image' => 'img/our-services/cfa-services.png',
                    ],
                    [
                        'title' => 'Turnkey Function Solutions',
                        'content' => '
                                    <div class="row mb-5">
                                        <div class="col-lg-12">
                                            <p>
                                                For businesses looking to outsource their entire finance function, we can provide all four layers of the finance function. For others, it may be more optimal to have a mix of internal and outsourced capability. We work with our clients to determine an optimal solution together and tailor our services to their needs.
                                            </p>
                                            <p>
                                                For an ideal outcome, you should consider ACOTA’s turnkey solution for building and delivering your finance function. We can manage all elements including:
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row mb-5">
                                        <div class="col-lg-12">
                                            <div class="row" id="why-icons">
                                                <div class="col-md-6 mb-2">
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <img class=" mb-2" src="http://localhost/acota/public/img/our-services/fo-1.png" width="100%">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-xs-10 mt-2">
                                                            Resource planning and
                                                            organisational design
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-2">
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <img class="mb-2" src="http://localhost/acota/public/img/our-services/fo-2.png" width="100%">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-xs-10  mt-2">
                                                            Sourcing and selecting
                                                            candidates

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-2">
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <img class=" mb-2" src="http://localhost/acota/public/img/our-services/fo-3.png" width="100%">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-xs-10  mt-2">
                                                            Establishing policies
                                                            and procedures

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-2">
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <img class="mb-2" src="http://localhost/acota/public/img/our-services/fo-4.png" width="100%">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10  col-xs-10  mt-2">
                                                            System implementation
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-2">
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <img class=" mb-2" src="http://localhost/acota/public/img/our-services/fo-5.png" width="100%">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-xs-10  mt-2">
                                                            Recruitment and training
                                                            of finance personnel
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-2">
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <img class="mb-2" src="http://localhost/acota/public/img/our-services/fo-6.png" width="100%">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10  col-xs-10  mt-2">
                                                            Ongoing management of
                                                            entire finance function
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        ',
                        'image' => 'img/our-services/entire-finance-function-outsourcing.png',
                    ],
                ]
            ],
            [
                'name' => 'OUTSOURCING',
                'content' => '<p>Whether you have a short-term project, temporary staffing needs or are looking to offshore positions permanently, ACOTA’s outsourcing solutions can support you through the journey with Project and Staff Outsourcing.</p>',
                'content_bottom' => '',
                'items' => [
                    [
                        'title' => 'Project Outsourcing:',
                        'content' => '
                                    <p>
                                        Clients can outsource specific projects for ACOTA to manage. For instance, during systems implementation, we can assist you with:
                                    </p>

                                    <div class="row" id="why-icons">
                                        <div class="col-md-6 mb-2">
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-xs-2">
                                                    <img class=" mb-2" src="http://localhost/acota/public/img/our-services/system.png" width="100%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-xs-10">
                                                    System selection and design
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-2">
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-xs-2">
                                                    <img class="mb-2" src="http://localhost/acota/public/img/our-services/user.png" width="100%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-xs-10">
                                                    User training
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-2">
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-xs-2">
                                                    <img class=" mb-2" src="http://localhost/acota/public/img/our-services/project.png" width="100%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-xs-10">
                                                    Project and vendor management
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-2">
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-xs-2">
                                                    <img class="mb-2" src="http://localhost/acota/public/img/our-services/documentation.png" width="100%">
                                                </div>
                                                <div class="col-md-10 col-sm-10  col-xs-10">
                                                    Documentation
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        ',
                        'image' => 'img/our-services/outsourcing.png',
                    ],
                    [
                        'title' => 'Staff Outsourcing:',
                        'content' => '
                                    <p>
                                       ACOTA can assist clients seeking to complement their onshore teams with offshore resources.    
                                       Through our staff outsourcing option, we can help you hire temporary or permanent staff offshore.
                                         Once onboarded, the staff would report directly and are managed by the client.  ACOTA can assist
                                          you with ongoing payroll and compliance activities relating to the staff, as well as any of 
                                          the following depending on your requirements:                                        
                                    </p>

                                    <div class="row mt-2" id="why-icons">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12 mb-2 mt-3">
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <img class=" mb-2" src="http://localhost/acota/public/img/our-services/cv.png" width="100%">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-xs-10">
                                                            CV sourcing
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mb-2">
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <img class="mb-2" src="http://localhost/acota/public/img/our-services/candidate.png" width="100%">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-xs-10">
                                                            Candidate interviews and selection
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-2 mt-3">
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-xs-2">
                                                    <img class="mb-2" src="http://localhost/acota/public/img/our-services/onboarding.png" width="100%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-xs-10">
                                                    Onboarding
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 mt-3">
                                                    An outsourced role will not only
                                                    provide cost-savings, but also allow
                                                    clients to directly supervise external
                                                    staff members as an extension of
                                                    their very own team.
                                                    The client retains responsibility
                                                    for orientation, training and
                                                    performance management.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
            
                        ',
                        'image' => 'img/our-services/staff.png',
                    ],
                ]

            ],
            [
                'name' => 'TRANSACTION ADVISORY',
                'content' => '<p>We go beyond the day to day. Our team and other specialists we work with, such as legal and specialist tax advisors, can assist you with valuation services, buying and selling a business, and raising debt capital.</p>',
                'content_bottom' => '<p>Depending on your circumstances, you may have specific advisors you wish to work with. ACOTA can work with your advisors or introduce you to the specialist skills you need among the network of professionals we work with.</p>',
                'items' => [
                    [
                        'title' => 'Valuation Services:',
                        'content' => '
                                    <p>
                                    Whether you are looking to buy a business, or sell your own, we can assist you in determining the value of that business.
                                     </p>
                        ',
                        'image' => 'img/our-services/valuation-service.png',
                    ],
                    [
                        'title' => 'Buying a Business',
                        'content' => '
                                    <p>
                                    We can support with target identification, deal strategy, commercial negotiations, due diligence, documentation and even post-acquisition integration.
                                     </p>
                        ',
                        'image' => 'img/our-services/buying-a-business.png',
                    ],
                    [
                        'title' => 'Selling a Business',
                        'content' => '
                                    <p>
                                    We can help you secure the maximum value for your business by working with you and your team to position the business for sale, identifying prospective buyers, commercial negotiation and being your eyes and ears through the documentation process.
                                     </p>
                        ',
                        'image' => 'img/our-services/selling-a-business.png',
                    ],
                    [
                        'title' => 'Raising Debt Capital',
                        'content' => '
                                    <p>
                                    Successful and profitable businesses may still require cash to supplement investor funds. We can source and negotiate debt capital, whether you are in the process of refinancing existing debt, raising fresh borrowings or managing debt existing debt. We help our clients by sourcing options from traditional banks and working capital providers, or via private debt markets as appropriate.
                                     </p>
                        ',
                        'image' => 'img/our-services/raising-debt-capital.png',
                    ],
                ],
            ],
        ];

        foreach ($data as $index => $item){
            $service['name'] = $item['name'];
            $service['content'] = $item['content'];
            $service['content_bottom'] = $item['content_bottom'];

            $newService = \App\Models\Service::create($service);

            foreach ($item['items'] as $ser){
                $newService->items()->create($ser);
            }
        }
    }
}
