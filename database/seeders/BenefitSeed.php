<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BenefitSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'Preparing Start-Ups',
                'content' => '
                    <p>
                        Businesses that are starting up come to us for a basic accounting system. We know that start-ups typically work with limited budgets and we can provide the most effective solution
                        to balance their books and finance operations. We take on responsibilities during the set-up stage and work with the client through the lifecycle of their business. We continue working with entrepreneurs and their management teams to build functionality and capability as the business grows in scale and complexity.
                    </p>
                    <p>
                        For clients who are starting off and do not have the budget or need for a full-time experience finance lead, ACOTA can work alongside you to design and build the systems and tools you need to make informed decisions.  Our offshore capability enables us to provide a cost-effective solution to maintain the systems and produce the reports and analytics you require. We free you up to focus on your core business, while ensuring you are getting the right data and advice along the way.
                    </p>
                ',
            ],
            [
                'title' => 'Planning for Exit',
                'content' => '
                    <p>
                        ACOTA seeks to partner with clients actively looking to build investment grade, exit-ready businesses. Our Transaction Advisory services will help you plan and execute your desired outcome.
                    </p>
                    <p>
                        Preparations for a successful exit start long before prospective sellers and buyers start discussions.
                        Established businesses who have started thinking about a potential exit scenario will need to curate their Management Information Systems (MIS) to help management deliver growth and demonstrate to potential investors the business’ achievements, as well as opportunities that lie ahead. Clients will also need to prepare themselves for the scrutiny of a due diligence process.
                    </p>
                    <p>
                        Even mature and established businesses can struggle with capacity constraints during a transaction as their teams continue to look after their day-to-day functions, as well as the extra requirements of a transaction. Moreover, clients may not have the capabilities to maximise the outcome of a transaction as the skill set of negotiating and managing a transaction are different from daily operations of
                        a finance functions. We provide our clients with options such as:
                    </p>
                    <p>&nbsp;</p>
                    <ul>
                        <li><p><img src="http://localhost/acota/public/img/why-us/long-term.png"> Long-term resources to build and maintain best-in-class finance functions</p></li>
                        <li><p><img src="http://localhost/acota/public/img/why-us/short-term.png"> Short-term resources to provide additional capacity through a transaction</p></li>
                        <li><p><img src="http://localhost/acota/public/img/why-us/valuable.png"> Valuable experience in mergers and acquisitions (M&amp;A)</p></li>
                    </ul>
                    <p>&nbsp;</p>
                    <p>
                        Speak to one of our partners if you are seeking assistance with commercial negotiations and financial due diligence through a Sale and Purchase Process.
                    </p>
                ',
            ],
            [
                'title' => 'Streamlining Finance Functions',
                'content' => '
                <p>Any organisation that loses their CFO or head of finance may be experiencing difficulties with the new change. We can evaluate the company’s finance function and recommend remedies to ensure a smoother transition.</p>
                ',
            ],
            [
                'title' => 'Creating Cost Savings',
                'content' => '
                <p>Even the best-run finance organisations in Australia can benefit from the cost-savings that come from outsourced solutions. Clients who are looking for margin improvements can benefit from offshoring one or more roles to India; with technology today and our workforce being adaptable to work to Australian hours, clients can achieve significant savings with gains in service delivery. Our trained team can also bring the analytical capability to help identify other potential efficiencies that can be targeted in our clients’ businesses.</p>
                ',
            ],
        ];

        foreach ($data as $index => $item){
            \App\Models\Benefit::create($item);
        }
    }
}
