<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $data = [
            [
                'name' => 'Home',
            ],
            [
                'name' => 'About Us',
            ],
            [
                'name' => 'Our Services',
            ],
            [
                'name' => 'Why Us',
            ],
            [
                'name' => 'Our Clients',
            ],
            [
                'name' => 'Our Team',
            ],
            [
                'name' => 'Terms and Conditions',
            ],
            [
                'name' => 'Privacy Policy',
            ],
            [
                'name' => 'Contact Us',
            ],
        ];


        $pageorder = 1;
        foreach ($data as $page){
            $pageData['name'] = $page['name'];
            $pageData['order'] = $pageorder;
            $pageData['slug'] = \Illuminate\Support\Str::slug($page['name']);

            $newpage = \App\Models\Page::create($pageData);

            $pageorder++;
        }
    }
}
