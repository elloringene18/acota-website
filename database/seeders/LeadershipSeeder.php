<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LeadershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Pedram Choroomi',
                'title' => 'CPA, LLB, MBA',
                'bio' => '<p>
                            Pedram has over 15 years of cross-industry experience working in senior finance and operational roles, assisting start-up and high-growth companies deliver shareholder value. He uses his accounting, legal and commercial training to help improve corporate governance, shape corporate strategy, and reduce execution risk. His industry experience covers health and wellness, liquor, facilities management, manufacturing, and contracting.
                        </p>
                        <p>
                            Pedram is adept at shaping strategy and executing programs to minimise adverse impacts and unlock further growth potential. He uses his formal accounting, legal and business training to assist CEOs and businesses chart a growth trajectory, while balancing and managing risk appropriately.
                        </p>',
                'photo' => 'img/about/pedram.png'
            ],
            [
                'name' => 'Balaji Viswanathan',
                'title' => 'BSC, FCMA, MBA',
                'bio' => '<p>
                            Balaji Viswanathan is a Founding Partner at ACOTA.  Balaji is a qualified management accountant (India and Australia) and holds an MBA from Manchester Business School in the UK. Based in India, he directly oversees ACOTA’s offshore team and ensures consistent service delivery for all our clients. He has experience working in India, the Middle East and Australia, having worked with global leading companies such as Hyundai Motors India Limited, Almarai Company (KSA), Transguard Group (UAE), and Myhealth Medical Group (Australia).
                        </p>
                        <p>
                            Working closely with CEOs and senior management, he has played a key role in formulating strategies and ensuring execution risks are minimised. He has helped transform finance functions by building service-oriented teams that work together efficiently.
                        </p>',
                'photo' => 'img/about/balaji.png'
            ],
        ];

        foreach ($data as $index => $item){
            \App\Models\Leadership::create($item);
        }
    }
}
