<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Pedram',
                'bio' => '<p>Pedram is a Certified Public Accountant (CPA Australia) with a Master of Business Administration (MBA) from The University of Manchester. He holds a Bachelor of Commerce (BCom) and a Bachelor of Laws (LLB) from the University of New South Wales and has over 15 years of cross-industry experience working in senior finance and operational roles. Outside of work, he enjoys travelling, reading, and football.</p>',
                'photo' => 'img/our-team/pedram.jpg',
            ],
            [
                'name' => 'Balaji',
                'bio' => '<p>Balaji is a qualified management accountant (India and Australia) with an MBA from The University of Manchester in the UK. He has 25 years’ experience working with global leading companies in India, the Middle East and Australia. Balaji enjoys spending time with his family, listening to music, and reading.</p>',
                'photo' => 'img/our-team/balaji.jpg',
            ],
            [
                'name' => 'Nandu',
                'bio' => '<p>Nandu holds a Bachelor of Commerce and is a Certified Management Accountant with over 30 years of experience in financial accounting, costing, budgeting, financial system and process design and implementation. He is well versed in ERP implementation, MIS, as well as statutory and internal audits that span multiple industries. Nandu is an avid reader and enjoys sports, as well as listening to music and religious discourse.</p>',
                'photo' => 'img/our-team/nandu.jpg',
            ],
            [
                'name' => 'Nirmala',
                'bio' => '<p>Nirmala holds a Master of Commerce and is a Certified Management Accountant (US). She has over 10 years of experience in bookkeeping, accounting and month-end reporting. She enjoys sketching and teaching.</p>',
                'photo' => 'img/our-team/nirmala.jpg',
            ],
            [
                'name' => 'Priya',
                'bio' => '<p>Priya holds a Bachelor of Commerce and a Masters Programme in International Business (MPIB). She has over 15 years of experience in bookkeeping, financial accounting and month-end reporting. In her spare time, she enjoys baking and listening to music.</p>',
                'photo' => 'img/our-team/priya.jpg',
            ],
            [
                'name' => 'Ravi',
                'bio' => '<p>Ravi holds a Bachelor of Commerce and has over 30 years of experience in financial accounting, banking, budgeting, auditing, payroll, taxation and foreign trade. He is a movie buff and enjoys cricket and reading.</p>',
                'photo' => 'img/our-team/ravi.jpg',
            ],
            [
                'name' => 'Subha',
                'bio' => '<p>Subha holds a Bachelor of Commerce and a Master of Business Administration. She has over five years of experience in bookkeeping and accounting. Her personal interests are photography, travel and music.</p>',
                'photo' => 'img/our-team/subha.jpg',
            ],
            [
                'name' => 'Bana',
                'bio' => '<p>Bana holds a Bachelor of Arts in Journalism and Political Science from Concordia University in Canada and a Master of Public Relations and Advertising from the University of New South Wales in Australia. She enjoys baking, photography, and has a passion for all things nature.</p>',
                'photo' => 'img/our-team/bana.jpg',
            ],
        ];

        foreach ($data as $index => $item){
            \App\Models\Team::create($item);
        }
    }
}
