<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'name' => 'Admin', 'email' => 'gene@thisishatch.com', 'password' => \Illuminate\Support\Facades\Hash::make('Hatch@2018!') ],
            [ 'name' => 'Admin', 'email' => 'webmaster@acota.com.au', 'password' => \Illuminate\Support\Facades\Hash::make('acota@admin_202!!$#') ],
        ];

        foreach ($data as $index => $item){
            \App\Models\User::create($item);
        }
    }
}
