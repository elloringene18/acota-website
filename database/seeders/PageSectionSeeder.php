<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PageSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'title','parent_sub_id','content'
        $data = [

                1 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'Scalable &amp; customised finance <br> function solutions for every business',
                    ],
                    [
                        'title' => 'Intro Content',
                        'content' => '
                                <p>
                                    <strong>At ACOTA</strong>, we provide a scalable service to assist with the day-to-day finance operations of our clients. We utilise professionally qualified offshore talent to provide a cost-effective service, while maintaining strict quality standards.
                                </p>
                                <p>
                                    With the latest technology and automation tools on hand, we work alongside our clients to build profitable businesses by helping them develop efficient and scalable processes.
                                </p>
                        ',
                    ],
                    [
                        'title' => 'Intro Image',
                        'content' => 'img/home/top-image.png',
                        'type' => 'image',
                    ],
                    [
                        'title' => 'Why Heading',
                        'content' => '
                                Why<br>ACOTA?
                        ',
                    ],
                    [
                        'title' => 'Why Content',
                        'content' => '
                                <p>
                                    We assist businesses achieve their ambitions by helping them develop a best-in-class finance function. We are passionate about, and experienced in, managing the specific challenges that start-up and growth businesses face.
                                </p>
                        ',
                    ],
                ],
                2 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'ACOTA:
                            <br>
                            Accounting, Outsourcing
                            <br>
                            &amp; Transaction Advisory',
                    ],
                    [
                        'title' => 'Intro Content',
                        'content' => ' <p><strong>It’s all in our name, ACOTA.</strong> We stand for Accounting, Outsourcing and Transaction Advisory, but we’re more than just that.
                        We are passionate about business and we help build lasting ones.</p>

                        <p>
                            ACOTA was established by Pedram Choroomi and Balaji Viswanathan, who have worked together in various roles in industry and
                            each held CFO positions across various industries. Throughout our careers we have supported start-up and growth businesses
                            through to maturity and exit. We founded ACOTA to help clients and businesses build their wealth.
                        </p>

                        <p>
                            Collectively, our experience spans a range of industries including health and wellness, facilities management, manufacturing, contracting, liquor, and food. This has been gained across several geographies and enterprises of various sizes and structures including sole traders, privately held groups, and publicly listed companies.
                        </p>',
                    ],
                    [
                        'title' => 'MV Heading',
                        'content' => 'Our Mission & Vision',
                        'type' => 'text'
                    ],
                    [
                        'title' => 'Mission',
                        'content' => '<h3>Mission</h3>
                        <p>Our purpose is to help our clients build wealth through business. Using our experience, we support our clients in achieving efficiencies by bringing together tools and insights that enable them to establish enduring, scalable businesses.</p>',
                    ],
                    [
                        'title' => 'Vision',
                        'content' => '
                        <h3>Vision</h3>
                        <p>To be the advisory team of choice, recognised for our adaptability and reliability amongst professional talent and entrepreneurs. We aim to be synonymous with successful start-up and high-growth businesses. </p>
                        ',
                    ],
                    [
                        'title' => 'Core Values Intro',
                        'content' => '
                        <h2 class="mt-4">Our core values</h2>
                                <p>At ACOTA, our never-ending quest is to add value to our clients.<br>
                                    We do this by seeking out the best <strong>talent</strong> and investing in their <strong>training</strong>.<br>
                                    We work as a <strong>team</strong> to identify and deploy the most context appropriate <strong>technology</strong> for our clients.<br>
                                    We do all of this with <strong>tenacity</strong>.
                                </p>',
                    ],
                    [
                        'title' => 'Core Values Image',
                        'content' => 'img/about/core-icon.png',
                        'type' => 'image'
                    ],
                    [
                        'title' => 'Core Value 1 Title',
                        'content' => 'Talent',
                        'type' => 'text'
                    ],
                    [
                        'title' => 'Core Value 1 Content',
                        'content' => '<p>We seek out talent with potential and passion.</p>
                                        <p>Through our recruitment process we priortise character, attitude and willingness to learn above all else.
                                        </p>',
                    ],
                    [
                        'title' => 'Core Value 2 Title',
                        'content' => 'Technology',
                        'type' => 'text'
                    ],
                    [
                        'title' => 'Core Value 2 Content',
                        'content' => ' <p>We leverage technology to deliver efficiency and consistent quality.</p>
                                        <p>We utilise technology to scale existing solutions and solve new challenges.</p>',
                    ],
                    [
                        'title' => 'Core Value 3 Title',
                        'content' => 'Training',
                        'type' => 'text'
                    ],
                    [
                        'title' => 'Core Value 3 Content',
                        'content' => '<p>We value learning and recognise it benefits our staff and clients</p>
                                        <p>We recognise technology and our environment evolves rapidly; we challenge ourselves to do so too.</p>',
                    ],
                    [
                        'title' => 'Core Value 4 Title',
                        'content' => 'Teamwork',
                        'type' => 'text'
                    ],
                    [
                        'title' => 'Core Value 4 Content',
                        'content' => '<p>We live by the maxim that "the whole is greater than the sum of it\'s parts" (Aristotle)</p>
                                        <p>We understand that meaningful collaboration, both internally and externally, is required to achieve optimal results.</p>',
                    ],
                    [
                        'title' => 'Core Value 5 Title',
                        'content' => 'Tenacity',
                        'type' => 'text'
                    ],
                    [
                        'title' => 'Core Value 5 Content',
                        'content' => ' <p>We are passionate about everything we do and do it in the best of our ability.</p>
                                        <p>In every situation we look for the opportunity to develop our talent and how we use technology to better serve our clients.</p>',
                    ],
                    [
                        'title' => 'Leadership Team Heading',
                        'content' => 'Leadership Team',
                        'type' => 'text'
                    ],
                    [
                        'title' => 'Leadership Team Content',
                        'content' => '<p>As experienced industry practitioners, ACOTA’s founders understand the practical challenges that come with running finance functions and growing businesses. With many years of working closely with consultants, we understand that clients can appreciate and benefit from straightforward, practical, commercially minded advice.
                        </p>',
                    ],
                ],

                4 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'WHY CHOOSE ACOTA?',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Intro Content',
                        'content' => '<p>
                            We have extensive experience in a multitude of industries, large and small, and understand the importance of balancing short-term budgets with long-term objectives. Our clients benefit from our partners’ 40+ years of experience across several industries and business life cycles.  Having successfully built and reconfigured numerous finance functions we are aware of the challenges and growing pains that start-up and entrepreneurial businesses experience.  We take a proactive and evolving approach to balance capability and budget.
                        </p>
                        <p>
                            Companies that work with us enjoy a seamless experience as we focus both on technical aspects of our service, as well as the client experience. Using a mix of off-shore and local resources, we can tailor our approach to the specific requirements of each engagement, while leveraging the cost-savings advantage of our offshore team to the benefit of our clients.
                        </p>
                        <p>
                            Working with ACOTA gives our clients the cost advantages of lower cost offshore resources coupled with the support and insight from our highly experienced team.
                        </p>',
                    ],
                    [
                        'title' => 'How Heading',
                        'content' => 'HOW YOUR BUSINESS<br/>
                            CAN BENEFIT FROM OUR SERVICE OFFERINGS
                        ',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'How Content',
                        'content' => '<p>
                            All businesses require the minimum of a basic bookkeeping service, with more complex needs in demand as they move up from bookkeeping to CFO services. One thing is certain, our clients value the effectiveness and benefits they derive from the finance functions we provide.
                            </p>
                        ',
                    ],
                    [
                        'title' => 'What Heading',
                        'content' => 'What sets ACOTA apart?',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'What Content',
                        'content' => '
                            <div class="col-md-6 mb-4">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <img class=" mb-2" src="//acota.com.au/public/img/why-us/what-1.png" width="100%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-xs-10 blue-text mt-2">
                                        Our practical skillset and broad industry
                                        experience with qualifications from various
                                        professional associations and solid
                                        foundations in management accounting and
                                        finance.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <img class="mb-2" src="//acota.com.au/public/img/why-us/what-2.png" width="100%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-xs-10 blue-text mt-2">
                                        We effectively tailor programmes to your
                                        back-office team with our access to a wide
                                        network of professional service providers.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <img class=" mb-2" src="//acota.com.au/public/img/why-us/what-3.png" width="100%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-xs-10 blue-text mt-2">
                                        We are an agile team that provides flexible
                                        commercial arrangements with consultants
                                        who can support you on a project basis or
                                        through the full lifecycle of your business.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <img class="mb-2" src="//acota.com.au/public/img/why-us/what-4.png" width="100%">
                                    </div>
                                    <div class="col-md-10 col-sm-10  col-xs-10 blue-text mt-2">
                                        We go beyond general compliance activities.
                                        Our CFO and Transaction Advisory knowledge
                                        transforms finance into a value-added function
                                        for our clients.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <img class=" mb-2" src="//acota.com.au/public/img/why-us/what-5.png" width="100%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-xs-10 blue-text mt-2">
                                        We offer considerable insight and experience
                                        across various regions, including Australia,
                                        the Indian subcontinent, and the Middle East.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <img class="mb-2" src="//acota.com.au/public/img/why-us/what-6.png" width="100%">
                                    </div>
                                    <div class="col-md-10 col-sm-10  col-xs-10 blue-text mt-2">
                                        We are able to deliver cost savings for our
                                        clients by complementing offshore resources
                                        with local talent.
                                    </div>
                                </div>
                            </div>
                        ',
                    ],
                ],
                5 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'CLIENTS WHO HAVE ACHIEVED<br/>THEIR FINANCE GOALS WITH ACOTA',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Intro Content',
                        'content' => '',
                    ],
                ],
                6 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'CLIENTS WHO HAVE ACHIEVED<br/>THEIR FINANCE GOALS WITH ACOTA',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Intro Content',
                        'content' => '',
                    ],
                ],
                7 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'ACOTA STANDARD TERMS AND CONDITIONS',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Intro Content',
                        'content' => '
                        <ol type="1">
                        <li><strong>OUR OBLIGATIONS</strong>
                        </li>
                        </ol>
                        <ol type="a">
                        <li>We are obliged to consider whether our clients create any threats to compliance with our Fundamental Principles and where we cannot reduce the risk to an acceptable level, we are obliged to cease the Engagement under the Code (section 320) to decline or cease the Engagement.</li>
                        <li>We have a duty to act in your best interests, unless this duty is inconsistent with our duty to act in the public interest.</li>
                        <li>If the Engagement includes financial reporting, we have a duty to identify non-compliance that may materially impact
your business’s financial integrity. We will first discuss our concerns with you, your internal auditor, management, or governance office holders. We may also decide, based on the imminence of a breach likely to cause substantial harm to third parties including the public, to notify a regulatory authority without raising our concerns with you first.
</li>
                        <li>We are responsible for maintaining records for a period of five years unless otherwise required by legislation.</li>
                        <li>We will perform procedures (guided by the APES suite of standards) required that are directly related to the Engagement consistent with our Fundamental Principles of integrity, objectivity, professional competence and due care, confidentiality, professional behaviour, and identifying, avoiding and dealing with conflicts of interests.</li>
                        <li>We will document sufficient and appropriate records of the procedures performed for the Engagement, which may be subject to quality review by CPA Australia under APES 320 Quality Control for Firms.</li>
                        </ol>
                        <p><strong>&nbsp;</strong></p>
                        <ol start="2" type="1">
                        <li><strong>YOUR OBLIGATIONS</strong>
                        </li>
                        </ol>
                        <ol type="a">
                        <li>You are responsible for full disclosure of all relevant information.</li>
                        <li>You are responsible for your own record keeping relating to your affairs.</li>
                        <li>You are responsible for the reliability, accuracy and completeness of the particulars and information provided to us, and, if the Engagement includes financial reporting, the accounting records and disclosures of all material and relevant information provided to us. Accordingly, any advice given to you is only an opinion based on our knowledge or your circumstances.</li>
                        <li>You are responsible for retaining paperwork for as long as legally required.</li>
                        </ol>
                        <p>&nbsp;</p>
                        <ol start="3" type="1">
                        <li><strong>INDEPENDENT CONTRACTOR RELATIONSHIP</strong>
                        </li>
                        </ol>
                        <ol type="a">
                        <li>It is expressly agreed that ACOTA is acting as an independent contractor in performing the services under this agreement and is not an employee of the company. ACOTA shall have full discretion in determining the activity and resources levels required in rendering the services under this Engagement Letter. The Company further acknowledges that ACOTA shall remain free to accept any other consulting engagements that are similar in nature to this Engagement.
</li>
                        </ol>
                        <p>&nbsp;</p>
                        <ol start="4" type="1">
                        <li><strong>NON-SOLICITATION OF EMPLOYEES</strong>
                        </li>
                        </ol>
                        <ol style="list-style:none">
                            <li>
                            From the beginning term of the engagement, and up to one year after the termination of the Engagement, neither
ACOTA nor the Client, or their affiliated entities, shall without the other party’s prior written consent, directly or indirectly:</li>
                        </ol>
                        <ol type="a">
                        <li>solicit or encourage any person to leave the employment or other service of the other party; or</li>
                        <li>hire any person who has left employment with ACOTA or the Client within the one-year period following the termination
of that person’s employment with ACOTA or the Client as the case may be.
</li>
                        </ol>
                        <p>&nbsp;</p>
                        <ol start="5" type="1">
                        <li><strong>THIRD PARTY INVOLVEMENT</strong>
                        </li>
                        </ol>
                        <ol type="a">
                        <li>We may from time to time engage third party specialist professionals and other public practitioners where warranted to obtain the advice you need or to assist us to provide our service to you. These may include cloud service providers and outsourced service providers.
</li>
                        <li>We will seek your consent if third party involvement is required.</li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <ol start="6" type="1">
                        <li><strong>OWNERSHIP OF MATERIALS</strong>
                        </li>
                        </ol>
                        <ol type="a">
                        <li>You own all original materials given to us.</li>
                        <li>We own all materials produced by us that resulted from our skill and attention to the extent that the materials produced by us incorporate any original materials you give to us.</li>
                        </ol>
                        <p>&nbsp;</p>
                        <ol start="7" type="1">
                        <li><strong>PRIVACY</strong>
                        </li>
                        </ol>
                        <ol type="a">
                        <li>Our collection, use and disclosure of your personal information  (<strong>PI</strong>) may be subject to the Privacy Act 1988 (Cth) and accordingly we will only collect PI about you that relates to the Engagement. We may disclose PI about you with your implied consent for the primary purpose of this Engagement or to third parties by express consent or as required by law. We rely on Dropbox for our file storage and sharing, which may result in this PI being stored overseas. If you would like to access any PI we might hold about you contact us on <a href="mailto:info@acota.com.au">info@acota.com.au</a>.</li>
                        <li>We may collect PI about you, your representatives, your clients, and others when we provide services to you. If we do, you agree to work with us to ensure that we both meet our respective obligations under the Privacy Act 1988 (Cth). Your obligations may include ensuring your privacy policy and contracts include a reference to your collection practices, how you will use the PI and that you may disclose the PI to an agent for public accounting services.</li>
                        <li>Your PI may be disclosed to CPA Australia Ltd and its service providers (if requested) for the purpose of conducting a quality review assessment on the services provided, which is aimed at maintaining high industry professional standards. CPA Australia Ltd will handle your personal information in accordance with the <a href="https://www.cpaaustralia.com.au/utilities/privacy/privacy-policy">CPA Australia Privacy Policy</a>.</li>
                        </ol>
                        <p>&nbsp;</p>
                        <ol start="8" type="1">
                        <li><strong>CONFIDENTIALITY</strong>
                        </li>
                        </ol>
                        <ol type="a">
                        <li>Confidential information furnished by the Client for use in performing the services mentioned above, is and shall remain the Client’s property. Subject to the exclusions below, ACOTA shall not, during the continuance of this Engagement or at any time thereafter, disclose, divulge or make public any of the confidential information other than as required to perform its services or by law. Notwithstanding the above, we may disclose details and records of the services provided to you to CPA Australia Ltd, (if requested) for the purposes of conducting a quality review assessment aimed at maintaining high industry professional standards.</li>
                        <li>Where information is shared to employees or contractors in performance of the service, each of the recipients shall also be bound by this confidentiality clause. This provision shall survive the termination of the Engagement. Confidential information shall not include (a) information that is or becomes part of the public domain other than by disclosure by ACOTA in violation of this Engagement letter, (b) that is rightfully obtained by ACOTA from third parties without a duty of confidentiality, or (c) that is required to be disclosed by law, statute or regulation.</li>
                        </ol>
                        <p>&nbsp;</p>
                        <ol start="9" type="1">
                        <li><strong>PROFESSIONAL INDEMNITY INSURANCE (PII)</strong>
                        </li>
                        </ol>
                        <ol type="a">
                        <li>We hold professional indemnity insurance of at least the minimum amount prescribed in the CPA Australia Ltd By-Laws or as required by law. Our PII cover at the time of this Engagement is $2,000,000.
</li>
                        </ol>
                        <p>&nbsp;</p>
                        <ol start="10" type="1">
                        <li><strong>PROFESSIONAL STANDARDS SCHEME & LIMITATION OF LIABILITY</strong>
                        </li>
                        </ol>
                        <ol type="a">
                        <li>We participate in the CPA Australia Ltd Professional Standards Scheme (Scheme), which facilitates the improvement of professional standards to protect consumers and may limit our liability to you in a cause of action. The Scheme applies to professional accounting services including accounting, bookkeeping, taxation, auditing and assurance, insolvency and corporate reconstruction, management accounting, management consulting, forensic accounting, valuation services.
</li>
                        <li>Notwithstanding anything to the contrary or to any other clause in this Engagement, to the extent permitted by law, the maximum liability of ACOTA under or arising out of and/or in relation to this Engagement is limited to 50% of the total fees paid to us in the 12 months immediately preceding the date on which the cause of action on which the claim is based first arose or $50,000, whichever is lower.</li>
                        <li>ACOTA shall have no liability in respect of (a) any consequential losses or (b) any other claim unless written notice of the claim is given to ACOTA specifying the matters giving rise to the claim within 7 days of such matters being known to the Client.</li>
                        <li>You agree not to pursue any claims against any other parties, including individual officers, contractors or employees of ACOTA, in respect of any alleged loss in connection with the provision of our services.</li>
                        <li>The above limitations do not apply to any claims or loss to the extent caused by any acts, omissions or representation made by us, which are criminal, dishonest, or fraudulent.</li>
                        </ol>
                        <p>&nbsp;</p>
                        
                                ',
                    ],
                ],
                8 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'Privacy Policy',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Intro Content',
                        'content' => '<ul>
                        <li>Our collection use and disclosure of your Personal Information may be subject to the Privacy Act 1988 (Cth) and accordingly, we will only collect Personal Information about you that relates to the Engagement.&nbsp; We may disclose Personal Information about you with your implied consent for the primary purpose of this Engagement or to third parties by express consent or as required by law.&nbsp; We rely on Dropbox for our file storage and sharing, which may result in this Personal Information being stored overseas. If you would like to access any Personal Information we might hold about you, please contact us at&nbsp;<a href="mailto:info@acota.com.au" target="_blank">info@acota.com.au</a>.</li>
                        <li>We may collect Personal Information about you, your representatives, your clients and others when we provide services to you. If we do, you agree to work with us to ensure that we both meet our respective obligations under the Privacy Act 1988 (Cth). Your obligations may include ensuring your privacy policy and contracts include a reference to your collection practices, how you will use the Personal Information and that you may disclose the Personal Information to an agent for public accounting services.&nbsp;</li>
                        <li>Your Personal Information may be disclosed to CPA Australia Ltd and its service providers (if requested) for the purpose of conducting a quality review assessment on the services provided, which is aimed at maintaining high industry professional standards. CPA Australia Ltd will handle your personal information in accordance with the&nbsp;<a href="https://www.cpaaustralia.com.au/utilities/privacy/privacy-policy" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.cpaaustralia.com.au/utilities/privacy/privacy-policy&amp;source=gmail&amp;ust=1614243558005000&amp;usg=AFQjCNHj3_lJUQS_a8jEBMAVLXib2wdUUw">CPA Australia Privacy Policy</a>.</li>
                        </ul>',
                    ],
                ],
                9 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'GET IN TOUCH WITH US',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Intro Content',
                        'content' => '<p>Leave a message and we will contact you as soon as we can.</p>',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Contact 1',
                        'content' => '
                            <strong>Pedram Choroomi</strong><br>
                                    pedram@acota.com.au<br>
                                    M: +61 411 536 951
                        ',
                    ],
                    [
                        'title' => 'Contact 2',
                        'content' => '
                            <strong>Balaji Viswanathan</strong><br>
                                    balaji@acota.com.au<br>
                                    M: +91 955 15 333 99
                        ',
                    ],
                ],
        ];

        foreach($data as $key=>$sub){
            foreach($sub as $item){
                $item['page_id'] = $key;
                $item['is_visible'] = isset($item['is_visible']) ? $item['is_visible'] : 1;
                $item['type'] = isset($item['type']) ? $item['type'] : 'html';
                $item['slug'] = \Illuminate\Support\Str::slug($item['title']);

                \App\Models\PageSection::create($item);
            }
        }
    }
}
