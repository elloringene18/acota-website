<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class HomeIconSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'photo' => 'img/home/agile.png',
                'photo_active' => 'img/home/agile-active.png',
                'content' => 'Agile and tailored programmes through the full business lifecycle.',
            ],
            [
                'photo' => 'img/home/cost.png',
                'photo_active' => 'img/home/cost-active.png',
                'content' => 'Cost effective transaction processing through qualified offshore talent.',
            ],
            [
                'photo' => 'img/home/insight.png',
                'photo_active' => 'img/home/insight-active.png',
                'content' => 'Insight and experience across various industries and regions.',
            ],
            [
                'photo' => 'img/home/value.png',
                'photo_active' => 'img/home/value-active.png',
                'content' => 'Investment grade management reporting and business partnering support.',
            ],
        ];

        foreach ($data as $index => $item){
            \App\Models\HomeIcon::create($item);
        }
    }
}
