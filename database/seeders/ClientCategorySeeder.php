<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Psy\Util\Str;

class ClientCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $data = [
            [
                'title' => 'Family Office',
                'items' => [
                    [
                        'name' => 'Rock Corp',
                        'photo' => 'img/our-clients/rockcorp.png',
                        'url' => 'http://www.mattrockman.com/',
                    ],
                ]
            ],
            [
                'title' => 'Food & Beverage',
                'items' => [
                    [
                        'name' => 'Lucky and Peps',
                        'photo' => 'img/our-clients/lucky-and-peps.png',
                        'url' => 'http://www.luckysandpeps.com.au/',
                    ],
                    [
                        'name' => 'Think Brands',
                        'photo' => 'img/our-clients/think-brands.png',
                        'url' => 'http://www.pampelle.com/',
                    ],
                ]
            ],
            [
                'title' => 'Health care',
                'items' => [
                    [
                        'name' => 'Cosmos Clinic',
                        'photo' => 'img/our-clients/cosmos.png',
                        'url' => 'https://www.cosmosclinic.com.au/',
                    ],
                    [
                        'name' => 'Care With Quality',
                        'photo' => 'img/our-clients/care-with-quality.png',
                        'url' => 'https://carewithquality.com.au/services/',
                    ],
                    [
                        'name' => 'SAI',
                        'photo' => 'img/our-clients/sai.png',
                        'url' => 'https://saihomecare.com.au/',
                    ],
                    [
                        'name' => 'Smart Clinics',
                        'photo' => 'img/our-clients/smart-clinics.png',
                        'url' => 'https://www.smartclinics.com.au/about/',
                    ],
                    [
                        'name' => 'Totally Smiles',
                        'photo' => 'img/our-clients/totally-smiles.png',
                        'url' => 'https://totallysmiles.com.au/about-us/',
                    ],
                ]
            ],
            [
                'title' => 'Private equity and financial services',
                'items' => [
                    [
                        'name' => 'White and Partners',
                        'photo' => 'img/our-clients/white-and-partners.png',
                        'url' => 'https://whiteandpartners.com.au/',
                    ],
                    [
                        'name' => 'Epsilon',
                        'photo' => 'img/our-clients/epsilon.png',
                        'url' => 'https://www.epsilondl.com.au/',
                    ],
                    [
                        'name' => 'Genesis Advisory',
                        'photo' => 'img/our-clients/genesis-advisory.png',
                        'url' => 'http://www.genesisadvisory.com.au/',
                    ],
                ]
            ],
            [
                'title' => 'Research and Development',
                'items' => [
                    [
                        'name' => 'Agilex',
                        'photo' => 'img/our-clients/agilex.png',
                        'url' => 'https://www.agilexbiolabs.com/',
                    ],
                    [
                        'name' => 'Avance',
                        'photo' => 'img/our-clients/avance.png',
                        'url' => 'https://www.avancecro.com/',
                    ],
                ]
            ],
            [
                'title' => 'Manufacturing',
                'items' => [
                    [
                        'name' => 'GMW',
                        'photo' => 'img/our-clients/gmw.png',
                        'url' => 'http://www.granitemarbleworks.com.au/',
                    ],
                ]
            ],
        ];

        foreach ($data as $index => $item){
            $d['title'] = $item['title'];
            $d['slug'] = \Illuminate\Support\Str::slug($item['title']);

            $cat = \App\Models\ClientCategory::create($d);

            foreach ($item['items'] as $ser){
                $cat->clients()->create($ser);
            }
        }
    }
}
